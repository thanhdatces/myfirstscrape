# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from bs4 import BeautifulSoup
import csv
import urllib
import pandas as pd

page = urllib.urlopen('https://otakusan.net/Manga/Index')
soup = BeautifulSoup(page, 'html.parser')
table = soup.find('div', attrs={'id': 'tags'})
results = table.find_all('div', attrs={'class': 'col-lg-2 col-md-3 col-sm-4 col-xs-6'})
print('Number of results', len(results))
rows = []
output_csv = pd.DataFrame(columns = ['Title', 'Rating', 'Type', 'Translator', 'Chapter'])

for result in results:
    Title = result.find('h4', attrs={'class': 'text-overflow'}).getText().replace('\n','').replace('\r','').strip()
    Rating = result.find('i', attrs={'class': 'icon-star-empty'}).getText().replace('\n','').replace('\r','').strip()
    TypeAndTranslater = result.find('div', attrs={'class': 'text-overflow-90'}).getText().replace('\n','').replace('\r','').strip()
    Type = TypeAndTranslater.split('-')[0]
    Translator = TypeAndTranslater.split('-')[1]
    Chapter = result.find('div', attrs={'class': 'mdl-card__actions mdl-card--border'}).getText().replace('\n','').replace('\r','').strip()
    rows.append({'Title':Title, 'Rating': Rating, 'Type': Type, 'Translator': Translator, 'Chapter': Chapter})

output_csv = output_csv.append(rows)
print(output_csv)
output_csv.to_dense().to_csv("otakuSanManga.csv", index = False, sep=',', encoding='utf-8')
